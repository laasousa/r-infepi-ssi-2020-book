`r if (knitr::is_html_output()) '# References {-}'`


<!-- 
---
nocite: '@*' 
---
allows for all references in .bib files to be included in the References chapter, 
even if no direct citation happens in the body of text.
Use it in Metadata option in main yaml setup
-->


